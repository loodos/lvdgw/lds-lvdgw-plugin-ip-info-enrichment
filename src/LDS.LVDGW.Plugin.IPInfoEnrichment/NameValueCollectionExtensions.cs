﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Primitives;

namespace LDS.LVDGW.Plugin.IPInfoEnrichment
{
    public static class NameValueCollectionExtensions
    {
        public static NameValueCollection AsNameValueCollection(this IDictionary<string, StringValues> collection)
        {
            NameValueCollection values = new NameValueCollection();
            foreach (KeyValuePair<string, StringValues> pair in collection)
            {
                string introduced3 = pair.Key;
                values.Add(introduced3, Enumerable.First<string>(pair.Value));
            }
            return values;
        }

        public static NameValueCollection AsNameValueCollection(this IEnumerable<KeyValuePair<string, StringValues>> collection)
        {
            NameValueCollection values = new NameValueCollection();
            foreach (KeyValuePair<string, StringValues> pair in collection)
            {
                string introduced3 = pair.Key;
                values.Add(introduced3, Enumerable.First<string>(pair.Value));
            }
            return values;
        }

        public static Dictionary<string, StringValues> AsDictionary(this NameValueCollection collection)
        {
            Dictionary<string, StringValues> dictValues = new Dictionary<string, StringValues>();
            foreach (var key in collection.AllKeys)
            {
                dictValues.Add(key, collection.Get(key));
            }

            return dictValues;
        }
    }
}
