﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.Enrichment;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Newtonsoft.Json;

namespace LDS.LVDGW.Plugin.IPInfoEnrichment
{
    /// http://ip-api.com/docs/api:json
    /// 

    public class Plugin : PluginBase, IEnrichmentPlugin
    {
        //public override string Name => "IPInfoEnrichment";

        [ParameterInfo("ReadIPFromHeader", "Read IP from xxx header", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "X-Real-IP")]
        public string ReadIPFromHeader { get; set; }

        [ParameterInfo("Container", "Client IP container", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "Header")]
        public string Container { get; set; }

        [ParameterInfo("Prefix", "Key prefix", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "X-IPINFO-")]
        public string Prefix { get; set; }

        public async Task EnrichAsync(HttpContext httpContext)
        {
            var clientIP = GetClientIP(httpContext);

            Log.Information("{ClientIP}", clientIP);

            AddToContainer(httpContext, "IP", clientIP);

            using (var client = new HttpClient())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"http://ip-api.com/json/{clientIP}");
                client.Timeout = TimeSpan.FromMilliseconds(300);

                HttpResponseMessage responseMessage = await client.SendAsync(requestMessage);

                if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string responseContent = await responseMessage.Content.ReadAsStringAsync();

                    var ipInfo = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseContent);
                    var status = ipInfo.GetValueOrDefault("status", "error").ToString();

                    if (status == "success")
                    {
                        foreach (var key in ipInfo.Keys)
                        {
                            AddToContainer(httpContext, key, ipInfo.GetValueOrDefault(key, "").ToString());
                        }
                    }
                    else
                    {
                        Log.Warning("IPInfoEnrichment error with {status} for {ClientIP}", status, clientIP);
                    }
                }
            }
        }

        private string GetClientIP(HttpContext httpContext)
        {
            if (!string.IsNullOrEmpty(ReadIPFromHeader) && httpContext.Request.Headers.Keys.Contains(ReadIPFromHeader, StringComparer.CurrentCultureIgnoreCase))
            {
                return httpContext.Request.Headers[ReadIPFromHeader].Last();
            }

            return httpContext.Connection.RemoteIpAddress.ToString();
        }

        private void AddToContainer(HttpContext httpContext, string key, string value)
        {
            var keyWithPrefix = Prefix + key;

            if (Container == "Header")
            {
                httpContext.Request.Headers.Add(keyWithPrefix, value);
            }
            else if (Container == "Form")
            {
                var collection = httpContext.Request.Form.AsNameValueCollection();

                collection.Add(keyWithPrefix, value);

                httpContext.Request.Form = new FormCollection(collection.AsDictionary());
            }
            else if (Container == "Query")
            {
                var collection = httpContext.Request.Query.AsNameValueCollection();

                collection.Add(keyWithPrefix, value);

                httpContext.Request.Query = new QueryCollection(collection.AsDictionary());
            }
        }

    }
}
